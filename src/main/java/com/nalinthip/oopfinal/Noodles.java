/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nalinthip.oopfinal;

/**
 *
 * @author ACER
 */
public class Noodles {
    private String name;
    private String noodlesoup;
    private String noodles;
    private String meatball;
    private String vegetable;

    public Noodles(String name, String noodlesoup, String noodles, String meatball, String vegetable) {
        this.name = name;
        this.noodlesoup = noodlesoup;
        this.noodles = noodles;
        this.meatball = meatball;
        this.vegetable = vegetable;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNoodlesoup() {
        return noodlesoup;
    }

    public void setNoodlesoup(String noodlesoup) {
        this.noodlesoup = noodlesoup;
    }

    public String getNoodles() {
        return noodles;
    }

    public void setNoodles(String noodles) {
        this.noodles = noodles;
    }

    public String getMeatball() {
        return meatball;
    }

    public void setMeatball(String meatball) {
        this.meatball = meatball;
    }

    public String getVegetable() {
        return vegetable;
    }

    public void setVegetable(String vegetable) {
        this.vegetable = vegetable;
    }

    @Override
    public String toString() {
        return "Noodles{" + "name = " + name + ", noodlesoup = " + noodlesoup + ", noodles = " + noodles + ", meatball = " + meatball + ", vegetable = " + vegetable + '}';
    }

    int size() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
       
}
